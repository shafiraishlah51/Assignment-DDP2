import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class GameTable{
    private JFrame panel;
    private ArrayList<GamePlayer> arrayGamePlayer = new ArrayList<>();
    private ArrayList<Integer> imageId = new ArrayList<>();
    private JPanel firstPanel = new JPanel();
    private JPanel midPanel = new JPanel();
    private GamePlayer card1;
    private GamePlayer card2;
    private Timer timer;
    private Timer openCard;
    private Timer breakTime;
    private int attemptsCount = 0;
    private JLabel playLabel;

    public GameTable(){
        panel = new JFrame("Memorize the picture and find the pair!");
        panel.setLayout(new BorderLayout());
        firstPanel.setLayout(new GridLayout(6,6));
        for (int i = 2; i < 20; i++) {
            imageId.add(i);
            imageId.add(i);
        }
        Collections.shuffle(imageId);
        for (int photos : imageId) {
            ImageIcon pic = new ImageIcon("C:\\Users\\User\\Documents\\Assignment DDP2\\assignment-4\\photo\\front.png");
            ImageIcon pic2 = new ImageIcon("C:\\Users\\User\\Documents\\Assignment DDP2\\assignment-4\\photo\\" + photos + ".png");
            pic = standardSize(pic);
            pic2 = standardSize(pic2);
            GamePlayer cardButton = new GamePlayer(photos, pic, pic2);
            cardButton.getButton().addActionListener(actions -> cardClicked(cardButton));
            arrayGamePlayer.add(cardButton);
        }
        createButton();
        createFooter();
        playLabel = new JLabel("Number of Attempts: " + attemptsCount);
        playLabel.setFont(new Font(playLabel.getFont().getName(), Font.PLAIN, 18));
        panel.add(firstPanel, BorderLayout.PAGE_START);
        panel.add(midPanel, BorderLayout.CENTER);
        panel.add(playLabel, BorderLayout.PAGE_END);
        panel.pack();
        panel.setVisible(true);
        panel.setResizable(false);
        panel.setSize(600, 700);
        panel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        breakTime = new Timer(2000, actions -> cardOpened());
        breakTime.start();
        breakTime.setRepeats(false);
    }

    public void createButton(){
        for (GamePlayer button : arrayGamePlayer) {
            firstPanel.add(button.getButton());
        }
    }
    public void createFooter() {
        JPanel gameFooter = new JPanel();
        gameFooter.setLayout( new GridLayout(1, 2));
        JButton nextGame = new JButton("Play again?");
        JButton exitGame = new JButton ("Exit");
        gameFooter.add(nextGame);
        gameFooter.add(exitGame);
        nextGame.addActionListener(actions -> newGame(arrayGamePlayer));
        exitGame.addActionListener(actions -> exitGame());
        midPanel.add(gameFooter);

    }
    public ImageIcon standardSize(ImageIcon picture){
        Image photo =  picture.getImage();
        Image resize = photo.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        return new ImageIcon(resize);
    }
    private void cardOpened() {
        for (GamePlayer i: arrayGamePlayer) {
            i.open();
        }
        openCard = new Timer(2500, actions -> cardHidden());
        openCard.setRepeats(false);
        openCard.start();
    }

    public void newGame(ArrayList<GamePlayer> cards) {
        panel.remove(firstPanel);
        Collections.shuffle(cards);
        firstPanel = new JPanel(new GridLayout(6,6));
        for (GamePlayer play : cards) {
            play.setIsIdentical(false);
            play.hide();
            play.getButton().setEnabled(true);
            firstPanel.add(play.getButton());
        }
        panel.add(firstPanel, BorderLayout.PAGE_START);
        attemptsCount = 0;
        playLabel.setText("Number of Attempts: " + attemptsCount );
        cardOpened();

    }

    public void cardHidden() {
        for (GamePlayer i : arrayGamePlayer) {
            i.hide();
        }
    }
    public void cardClicked(GamePlayer show) {
        if (card1 == null && card2 == null) {
            card1 = show;
            card1.open();
        }

        if (card1 != null && card1 != show && card2 == null) {
            card2 = show;
            card2.open();
            timer = new Timer(700, actions -> validCard());
            timer.setRepeats(false);
            timer.start();
        }
    }

    private void exitGame() {
        System.exit(0);
    }
    public void validCard() {
        if (card1.getId() == card2.getId()) {
            pairs();
            if (donePlaying()) {
                Object[] gameButton = {" Yes, I'm Done", " No, Play Again"};
                int choice = JOptionPane.showOptionDialog(null, "Are You Sure You Want to Quit? ", "Congratulations! You Win! ",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, gameButton, gameButton[1]);
                if (choice == 1) {
                    newGame(arrayGamePlayer);
                } else {
                    exitGame();
                }
            }
        } else {
            card1.hide();
            card2.hide();
            attemptsCount++;
            playLabel.setText("Number of Attempts: " + attemptsCount);
        }
        card1 = null;
        card2 = null;
    }

   public void pairs() {
        card1.getButton().setEnabled(false);
        card2.getButton().setEnabled(false);
        card1.setIsIdentical(true);
        card2.setIsIdentical(true);
    }
    public boolean donePlaying() {
        for (GamePlayer i: arrayGamePlayer) {
            if (!i.getIsIdentical()) {
                return false;
            }
        }
        return true;
    }


    /*public JPanel getPanel(){
        return panel;
    }*/

}