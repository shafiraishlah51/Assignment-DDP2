import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GamePlayer {
    private int id;
    private ImageIcon closed;
    private ImageIcon opened;
    private boolean isIdentical = false;
    private JButton button = new JButton();

    public GamePlayer(int id, ImageIcon closed, ImageIcon opened) {
        this.id = id;
        this.closed = closed;
        this.opened = opened;
        this.button.setIcon(closed);
        this.button.setPreferredSize(new Dimension(100, 100));
    }

    public void hide() {
        button.setIcon(closed);
    }

    public void open() {
        button.setIcon(opened);
    }

//    public void isRevealed() {
//        button.setPreferredSize(new Dimension(100, 100));
//        if (isClicked) {
//            button.setIcon(revealed);
//        } else {
//            button.setIcon(closed);
//        }
//    }

    public void setIsIdentical(boolean match) { this.isIdentical = match; }

    public ImageIcon getClosed() { return closed; }

    public ImageIcon getOpened() { return opened; }

    public int getId() { return id; }

    public JButton getButton() { return button; }

    public boolean getIsIdentical() { return isIdentical; }
}