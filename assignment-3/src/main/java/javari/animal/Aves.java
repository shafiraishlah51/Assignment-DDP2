package javari.animal;

import java.util.Scanner;
import javari.animal.*;
import java.util.Arrays;

public class Aves extends Animal{
    
    private static String[] aves = {"Parrot", "Eagle"};
    private boolean isLayingEggs = false;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, String layeggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if(layeggs.equalsIgnoreCase("laying eggs")){
            this.isLayingEggs = true;
        }
    }

    public boolean specificCondition(){
        return !(isLayingEggs);//yg boleh yg ga bertelur
    }

    public static String[] getAves(){
        return aves;
    }


}