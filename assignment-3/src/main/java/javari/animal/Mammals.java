package javari.animal;

import java.util.Scanner;
import javari.animal.*;
import java.util.Arrays;


public class Mammals extends Animal{
    
    private static String[] mammalia = {"Cat", "Whale", "Hamster", "Lion"};//anggota mammals
    private boolean isPregnant = false; //assume not pregnant

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, String preg, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if(preg.equalsIgnoreCase("pregnant")){//kalo mammalsnya hamil
            this.isPregnant = true;
        }
    }

    public boolean specificCondition(){
        return !(isPregnant) && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));//yg boleh yg ga hamil dan (bukan singa or gender cowo)
    }

    public static String[] getMamalia(){
        return mammalia;
    }

}