package javari.reader;

import javari.park.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

public class AnimalAttractions extends CsvReader{
    
    public AnimalAttractions(Path file) throws IOException{
        super(file);
    }

    public long countValidRecords(){
        Set<String> valid = new LinkedHashSet<>();//Constructs a new, empty linked hash set with the specified initial capacity and the default load factor (0.75).
        for(int x = 0; x < lines.size(); x++){
            String baris = lines.get(x);
            String[] barisSplit = baris.split(",");
            if(Arrays.asList(Attractions.getValidAttractions()).contains(barisSplit[1])){//atraksi hewan
                switch(barisSplit[1]){
                    case "Circles of Fires":
                        if(CirclesOfFires.checkValidity(barisSplit[0])){//cocok ga atraksi sma hewannya
                            new Attractions("Circles of Fires", barisSplit[0]);
                            valid.add(barisSplit[1]);
                        }
                        break;
                    case "Counting Masters":
                        if(CountingMasters.checkValidity(barisSplit[0])){
                            new Attractions("Counting Masters", barisSplit[0]);
                            valid.add(barisSplit[1]);
                        }
                        break;
                    case "Dancing Animals":
                        if(DancingAnimals.checkValidity(barisSplit[0])){
                            new Attractions("Dancing Animals", barisSplit[0]);
                            valid.add(barisSplit[1]);
                        }
                        break;
                    case "Passionate Coders":
                        if(PassionateCoders.checkValidity(barisSplit[0])){
                            new Attractions("Passionate Coders", barisSplit[0]);
                            valid.add(barisSplit[1]);
                        }
                        break;
                }
                
            }
        }

        return (long)valid.size();//return banyaknya yg valid 
    }

    public long countInvalidRecords(){
        long invalid = 0;
        for(int x = 0; x < lines.size(); x++){
            String baris = lines.get(x);
            String[] barisSplit = baris.split(",");
            if(Arrays.asList(Attractions.getValidAttractions()).contains(barisSplit[1])){
                switch(barisSplit[1]){
                    case "Circles of Fires":
                        if(CirclesOfFires.checkValidity(barisSplit[0]) == false){//kalo atraksi sama hewan ga cocok
                            invalid += 1;
                        }else{
                            continue;
                        }
                    case "Counting Masters":
                        if(CountingMasters.checkValidity(barisSplit[0]) == false){
                            invalid += 1;
                        }else{
                            continue;
                        }
                    case "Dancing Animals":
                        if(DancingAnimals.checkValidity(barisSplit[0]) == false){
                            invalid += 1;
                        }else{
                            continue;
                        }
                    case "Passionate Coders":
                        if(PassionateCoders.checkValidity(barisSplit[0]) == false){
                            invalid += 1;
                        }else{
                            continue;
                        }
                }
                
            }else{
                invalid += 1;
            }
        }
        return invalid;
    }
}