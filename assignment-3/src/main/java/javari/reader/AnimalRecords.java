package javari.reader;

import javari.animal.*;
import javari.park.*;
import javari.reader.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class AnimalRecords extends CsvReader{

    long valid = 0;
    int counter = 0;

    public AnimalRecords(Path file) throws IOException{
        super(file);
    }
    
    //membuat method untuk memproses record-record dari file
    public void count(){
        for(int x = 0; x < lines.size(); x++){
            Animal z = null;
            String masukan = lines.get(x);
            String[] masukanSplit = masukan.split(",");
            Integer a =  Integer.valueOf(masukanSplit[0]);
            String b = masukanSplit[1];//spesies hewan
            String c = masukanSplit[2];//nama hewan
            Gender d = Gender.parseGender(masukanSplit[3]);//gender
            double e = Double.parseDouble(masukanSplit[4]);//panjang
            double f = Double.parseDouble(masukanSplit[5]);//berat
            String g = masukanSplit[6];//specialcondition(hamil/bertelur)
            Condition h = Condition.parseCondition(masukanSplit[7]);//condition(sehat/nggak)
            if(Arrays.asList(Mammals.getMamalia()).contains(b)){
                z = new Mammals(a,b,c,d,e,f,g,h);
                valid += 1;
            }else if(Arrays.asList(Aves.getAves()).contains(b)){
                z = new Aves(a,b,c,d,e,f,g,h);
                valid += 1;
            }else if(Arrays.asList(Reptiles.getReptilia()).contains(b)){
                z = new Reptiles(a,b,c,d,e,f,g,h);
                valid += 1;
            }
            //menambahkan animal baru ke dalam atraksi-atraksi dengan tipe yang sesuai
            for(int y = 0; y < Attractions.getAttractionsList().size(); y++){
                if(Attractions.getAttractionsList().get(y).getType().equalsIgnoreCase(b)){
                    Attractions.getAttractionsList().get(y).addPerformer(z);
                }
            }

        }
        counter += 1;
    }

    public long countValidRecords(){//ngitung valinya
        if(counter == 0){
            count();
        }
        return valid;
    }

    public long countInvalidRecords(){
        return lines.size() - countValidRecords();
    }
    
}