package javari.park;

import java.util.List;
import java.util.ArrayList;
import javari.park.*;
import javari.animal.*;
import javari.park.*;

public class Attractions implements SelectedAttraction{
    
    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<Animal>();//yg udh fix tampil
    private static ArrayList<Attractions> listOfAttractions = new ArrayList<Attractions>();
    private static String[] validCategories = {"Explore the Mammals", "Reptillian Kingdom", "World of Aves"};
    private static String[] validAttractions = {"Circles of Fires", "Counting Masters", "Dancing Animals", "Passionate Coders"};
    private ArrayList<Attractions> attractions = new ArrayList<Attractions>();
    
    public Attractions(){

    }
    public Attractions(String name, String type) {//nama hewan, atraksi
        this.name = name;
        this.type = type;
        listOfAttractions.add(this);
    }
    public static String[] getValidCategories(){
        return validCategories;
    }

    public static String[] getValidAttractions(){
        return validAttractions;
    }
    
    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Animal> getPerformers() {
        return performers;
    }

    public static ArrayList<Attractions> getAttractionsList() {
        return listOfAttractions;
    }

    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {//kondisi klo bisa tampil
            performers.add(performer);
            return true;
        }
        return false;//klo hewannya ga bisa tampil
    }

    public ArrayList<Attractions> findAnimal(String animal){//nyari hewannya
        for(int x = 0; x < listOfAttractions.size(); x++){
            if(listOfAttractions.get(x).getType().equalsIgnoreCase(animal)){
                attractions.add(listOfAttractions.get(x));
            }
        }
        return attractions;
    }

}