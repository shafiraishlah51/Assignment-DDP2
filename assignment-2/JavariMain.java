import animal.*;
import cage.*;

import java.util.Scanner;
import java.util.ArrayList;

class JavariMain{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args){
		System.out.println("Welcome to Javari Park!" + 
			"\nInput the Number of Animals");
		CageTools.createCage(Animals.create("cat"), "cat");
		CageTools.createCage(Animals.create("lion"), "lion");
		CageTools.createCage(Animals.create("eagle"), "eagle");
		CageTools.createCage(Animals.create("parrot"), "parrot");
		CageTools.createCage(Animals.create("hamster"), "hamster");
		
		System.out.print("\nAnimals has been successfully recorded!"
		+ "\n============================================="
		+ "\nCage arrangement:");
		
		CageTools.printCage("cat");
		CageTools.printCage("lion");
		CageTools.printCage("eagle");
		CageTools.printCage("parrot");
		CageTools.printCage("hamster");
		
		System.out.println("\nANIMALS NUMBER:");
		
		System.out.println("cat:" + CageTools.countCage("cat"));
		System.out.println("lion:" + CageTools.countCage("lion"));
		System.out.println("eagle:" + CageTools.countCage("eagle"));
		System.out.println("parrot:" + CageTools.countCage("parrot"));
		System.out.println("hamster:" + CageTools.countCage("hamster"));
		
		System.out.print("============================================= ");
		while(true){
			System.out.println("\nWhich animal you want to visit?\n" + 
			"(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
			String command = input.nextLine();
			switch(command){
				case "1":
				CageTools.visitCage("cat");
				break;
				case "2":
				CageTools.visitCage("eagle");
				break;
				case "3":
				CageTools.visitCage("hamster");
				break;
				case "4":
				CageTools.visitCage("parrot");
				break;
				case "5":
				CageTools.visitCage("lion");
				break;
				case "99":
				System.out.println("Leaving Javari Park");
				System.exit(0);
				break;
				default :
				System.out.println("Wrong command");
				break;
			}
		}
	}
}