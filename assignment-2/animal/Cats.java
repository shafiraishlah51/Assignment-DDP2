package animal;

import java.util.Random;
import java.util.Scanner;

public class Cats extends Animals{
	
	public Cats(String name, int length){
		super(name, length, "pet");
	}

	//Method untuk sound ketika cat brushfur
	public void brushFur(){
		System.out.println("Time to clean " + this.name + "'s fur");
		System.out.println(this.name + " makes a voice: Nyaaan...");
	}
	
	//Method untuk sound ketika cat cuddle
	public void cuddle(){
		Random random = new Random();
		String[] sounds = {"Miaaaw.." , "Purrr.." , "Mwaw!" , "Mraaawr!"};
		System.out.println(this.name + " makes a voice: " + sounds[random.nextInt(4)]);
	}
	
	//Method untuk mngatur perintah dari inputan
	public void visit(String name){
		Scanner input = new Scanner(System.in);
		System.out.println("1: Brush the fur 2: Cuddle");
		String command = input.nextLine();
		if(command.equals("1")){
			this.brushFur();
		}
		else if(command.equals("2")){
			this.cuddle();
		}
		else{
			System.out.println("You do nothing...");
		}
	}
}