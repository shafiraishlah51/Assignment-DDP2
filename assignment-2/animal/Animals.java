package animal;

import java.util.Scanner;

public class Animals{
	protected String name;
	protected int length;
	private String kind;
	
	public Animals(String name, int length, String kind){
		this.name = name;
		this.length = length;
		this.kind = kind;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getLength(){
		return this.length;
	}
	
	public String getKind(){
		return this.kind;
	}
	
	public static Animals[] create(String species){
		Scanner input = new Scanner(System.in);
		System.out.print(species + ": ");
		int number = Integer.parseInt(input.nextLine());
		if(number == 0){
			return null;
		}
		else{
			System.out.println("Provide the information of " + species + "(s):");
			String[] info = input.nextLine().split(",");
			Animals[] pack = new Animals[info.length];
			for(int i = 0; i < info.length; i++){
				String[] detail = info[i].split("\\|");	
				switch(species){
					case "cat" : 
					pack[i] = new Cats(detail[0], Integer.parseInt(detail[1]));
					break;
					case "eagle" : 
					pack[i] = new Eagles(detail[0], Integer.parseInt(detail[1]));
					break;
					case "hamster" : 
					pack[i] = new Hamsters(detail[0], Integer.parseInt(detail[1]));
					break;
					case "parrot" : 
					pack[i] = new Parrots(detail[0], Integer.parseInt(detail[1]));
					break;
					case "lion" : 
					pack[i] = new Lions(detail[0], Integer.parseInt(detail[1]));
					break;
				}
			}
			return pack;
		}
	}
	
	public static void visit(Animals animal, String name, String species){
		if(species.equals("cat")){
			((Cats)animal).visit(name);
		}
		else if(species.equals("eagle")){
			((Eagles)animal).visit(name);
		}
		else if(species.equals("hamster")){
			((Hamsters)animal).visit(name);
		}
		else if(species.equals("parrot")){
			((Parrots)animal).visit(name);
		}
		else{
			((Lions)animal).visit(name);
		}
		System.out.println("Back to office!");
	}	
}