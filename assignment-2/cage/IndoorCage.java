package cage;
import animal.*;

class IndoorCage extends Cage{
	
	public IndoorCage(Animals animal){
		super(animal, "indoor");
	}
	
	//Method untuk mengelompokkan typeCage untuk yg indoor
	public static char cageType(int length){
		if(length < 45){
			return 'A';
		}
		else if(length <= 60){
			return 'B';
		}
		else{
			return 'C';
		}
	}
}