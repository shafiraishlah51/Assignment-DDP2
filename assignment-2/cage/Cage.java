package cage;
import animal.*;

public class Cage{
	protected Animals animal;
	protected char type;
	
	public Cage(Animals animal, String cage){
		this.animal = animal;
		if(cage.equals("indoor")){
			this.type = IndoorCage.cageType(animal.getLength());
		}
		else{
			this.type = OutdoorCage.cageType(animal.getLength());
		}
	}
	
	public Animals getAnimal(){
		return this.animal;
	}
	
	public char getType(){
		return this.type;
	}
}