package cage;
import animal.*;

class OutdoorCage extends Cage{

	public OutdoorCage(Animals animal){
		super(animal, "outdoor");
	}
	
	//Method untuk mengelompokkan Typecage yang outdoor
	public static char cageType(int length){
		if(length < 75){
			return 'A';
		}
		else if(length <= 90){
			return 'B';
		}
		else{
			return 'C';
		}
	}
}