public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    public WildCat cat;
    public TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        if (next == null){
            return cat.weight + EMPTY_WEIGHT;

        } else {
            return cat.weight + EMPTY_WEIGHT + next.computeTotalWeight();
            
        }
        
    }

    public double computeTotalMassIndex() {
        if (this.next == null){
            return cat.computeMassIndex();
        }else{
            return cat.computeMassIndex()+this.next.computeTotalMassIndex();
        }
        
    }

    public void printCar() {
        if (this.next == null){
            System.out.println("("+this.cat.name+")"); 
        }else{
            System.out.print("("+this.cat.name+")--");
            this.next.printCar();
        }
    }
}
